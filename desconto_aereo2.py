#Gustavo Sousa de Lira RA: 21908311

def valorPassagem():
    valor = float(input('Valor da Passagem: R$'))
    return valor

def tipoPassagem():
    print(f'[1] Nacional \n[2] Internacional')
    tipo = input('>>> ')
    if tipo == '1': tipo = 'Nacional'
    if tipo == '2': tipo = 'Internacional'
    return tipo

def classePassagem():
    print(f'[1] Executiva \n[2] Econômica')
    classe = input('>>> ')
    if classe == '1': classe = 'Executiva'
    if classe == '2': classe = 'Econômica'
    return classe

def resultado(valor, tipo, classe):
    global desconto
    if tipo == 'Nacional' and classe == 'Executiva':
        desconto = 25
        valor = valor * (1 - desconto/100)
    if tipo == 'Nacional' and classe == 'Econômica':
        desconto = 15
        valor = valor * (1 - desconto/100)
    if tipo == 'Internacional' and classe == 'Executiva':
        desconto = 40
        valor = valor * (1 - desconto/100)
    if tipo == 'Internacional' and classe == 'Econômica':
        desconto = 30
        valor = valor * (1 - desconto/100)
    return valor

valor, tipo, classe, desconto = '', '', '', ''

valor = valorPassagem()
tipo = tipoPassagem()
classe = classePassagem()
resultado = resultado(valor, tipo, classe)

print(f'Desconto de {desconto}%')
print(f'Novo preço: R${round(resultado, 2)}')
input()