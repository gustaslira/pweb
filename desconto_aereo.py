#Gustavo Sousa de Lira RA: 21908311

def selector(n1, n2):
    print(f'[1] {n1} \n[2] {n2}')
    var = input('>>> ')
    return var
    
def descontoPassagem(t_nome, c_nome, dsc):
    global tipo, classe, desconto, passagem
    tipo = t_nome
    classe = c_nome
    desconto = dsc
    passagem = passagem * (1 - dsc/100)

original = float(input('Valor da passagem: R$'))
passagem = original
desconto = 0

tipo = selector('Nacional', 'Internacional')

if tipo == '1': 
    classe = selector('Executiva', 'Econômica')
    if classe == '1': descontoPassagem('Nacional', 'Executiva', 25)
    if classe == '2': descontoPassagem('Nacional', 'Econômica', 15)

if tipo == '2': 
    classe = selector('Executiva', 'Econômica')
    if classe == '1': descontoPassagem('Internacional', 'Executiva', 40)
    if classe == '2': descontoPassagem('Internacional', 'Econômica', 30)

print(f'\nPreço Original: R${original}')
print(f'Desconto de {desconto}%')
print(f'Novo Preço: R${round(passagem, 2)}')
print(f'Tipo de voô: {tipo}')
print(f'Classe do Voô: {classe}')
input()